# LARA-django-app Cookiecutter

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

    pip install -U cookiecutter

Generate a Python package project::

     python -m cookiecutter gl:larasuite/lara-django-app-cookiecutter

Then:

  * Create a repo and put it there.
  * Install the dev requirements into a virtualenv.
  * Release your package by pushing a new tag to master.
  * Get your code on!


## Acknowledgements

  * Daniel Roy Greenfeld  <a href="https://github.com/pydanny">pydanny</a> and his cookiecutter-django team

