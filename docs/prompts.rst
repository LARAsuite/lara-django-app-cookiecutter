Prompts
=======

When you create a package, you are prompted to enter these values.

Templated Values
----------------

The following appear in various parts of your generated project.

full_name
    Your full name.

email
    Your email address.

project_name
    The name of your new Python package project. This is used in documentation, so spaces and any characters are fine here.

project_slug
    The namespace of your Python package. This should be Python import-friendly. Typically, it is the slugified version of project_name.

project_short_description
    A 1-sentence description of what your Python package does.

project_gitlab_path
    The GitLab path for the project (default: ``genomicsengland/opensource/templates/<project-slug>``).

release_date
    The date of the first release.

version
    The starting version number of the package.

Options
-------

The following package configuration options set up different features for your project.

create_author_file
    Whether to create an AUTHORS file (default: ``n``).

use_mypy
    Whether to use mypy for type hinting validation (default: ``n``).

use_pylint
    Whether to use pylint for linting (default: ``n``).

line_length
    Maximum line length for formatting and validation (default: ``120``).

pypi_url
    The URL of the Python Package Index to use (default: ``https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple``).

open_source_license
    The license to use for the project (options: ``Not open source`` (default), ``MIT``, ``BSD-3-Clause``, ``ISC``, ``Apache-2.0``, ``GPL-3.0-only``).
