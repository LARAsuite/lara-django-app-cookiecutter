Tutorial
========

.. note:: Did you find any of these instructions confusing? `Edit this file`_
          and submit a pull request with your improvements!

.. _`Edit this file`: https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage/-/blob/master/docs/tutorial.rst


Step 1: Install Cookiecutter
----------------------------

Install cookiecutter:

.. code-block:: bash

    pip install cookiecutter

We'll also need to `install poetry`_.

.. _`install poetry`: https://python-poetry.org/docs/#installation

Step 2: Generate Your Package
-----------------------------

Now it's time to generate your Python package.

Use cookiecutter, pointing it at the cookiecutter-pypackage repo:

.. code-block:: bash

    cookiecutter https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage.git

You'll be asked to enter a bunch of values to set the package up.
If you don't know what to enter, stick with the defaults.


Step 3: Create a GitLab Repo
----------------------------

Go to your GitLab account and create a new repo named ``project_name``, where ``project_name`` matches the ``[project_slug]`` from your answers to running cookiecutter. This is so that Travis CI and pyup.io can find it when we get to Step 5.

You will find one folder named after the ``[project_slug]``. Move into this folder, and then setup git to use your GitHub repo and upload the code:

.. code-block:: bash

    cd mypackage
    git init .
    git add .
    git commit -m "Initial skeleton."
    git remote add origin git@gitlab.com:project_path/project_name.git
    git push -u origin master

Where ``project_path`` and ``project_name`` are adjusted for your username and package name.

Step 4: Install Dev Requirements
--------------------------------

You should still be in the folder containing the ``pyproject.toml`` file.

Install the new project's local development requirements inside a virtual environment using pipenv:

.. code-block:: bash

    poetry install

Step 5: Release on PyPI
-----------------------

The Python Package Index or `PyPI`_ is the official third-party software repository for the Python programming language. Python developers intend it to be a comprehensive catalog of all open source Python packages.

GEL maintains an `internal package repository`_, which is a mirror of the PyPI repository but with additional GEL-specific packages.

When you are ready, release your package the standard Python way.

See `PyPI Help`_ for more information about submitting a package.

.. _`PyPI`: https://pypi.python.org/pypi
.. _`internal package repository`: https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple
.. _`PyPI Help`: http://peterdowns.com/posts/first-time-with-pypi.html
