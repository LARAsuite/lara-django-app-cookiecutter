"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} tests *

:details: {{cookiecutter.project_slug}} application urls tests.
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }}  <{{ cookiecutter.email }}>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from {{cookiecutter.project_slug}}.models import

# Create your {{cookiecutter.project_slug}} tests here.
