"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} tests *

:details: {{cookiecutter.project_slug}} application views tests.
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }}  <{{ cookiecutter.email }}>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

import pytest
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest, HttpResponseRedirect
from django.test import RequestFactory
from django.urls import reverse


# from {{cookiecutter.project_slug}}.models import

# Create your {{cookiecutter.project_slug}} tests here.

pytestmark = pytest.mark.django_db
