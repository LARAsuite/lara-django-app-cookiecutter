"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} admin *

:details: {{cookiecutter.project_slug}} admin module admin backend configuration.
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }} <{{ cookiecutter.email }}>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator {{cookiecutter.project_slug}} >> admin.py" to update this file
________________________________________________________________________
"""
