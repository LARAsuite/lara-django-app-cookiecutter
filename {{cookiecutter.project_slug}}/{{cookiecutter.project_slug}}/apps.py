"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} app *

:details: {{cookiecutter.project_slug}} app configuration. 
         This provides a generic django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/4.0/ref/applications/
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }} <{{ cookiecutter.email }}>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


from django.apps import AppConfig


class {{cookiecutter.project_name_camel_case}}Config(AppConfig):
    name = '{{cookiecutter.project_slug}}'
    # enter a verbose name for your app: {{cookiecutter.project_slug}} here - this will be used in the admin interface
    verbose_name = '{{cookiecutter.project_name}}'
    # lara_app_icon = '{{cookiecutter.project_slug}}_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.
