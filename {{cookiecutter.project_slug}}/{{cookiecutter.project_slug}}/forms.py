"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} admin *

:details: {{cookiecutter.project_slug}} admin module admin backend configuration.
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }} <{{ cookiecutter.email }}>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c {{cookiecutter.project_slug}} > forms.py" to update this file
________________________________________________________________________
"""
