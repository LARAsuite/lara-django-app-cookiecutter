"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} admin *

:details: {{cookiecutter.project_slug}} admin module admin backend configuration.
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }} <{{ cookiecutter.email }}>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator {{cookiecutter.project_slug}} >> tables.py" to update this file
________________________________________________________________________
"""
