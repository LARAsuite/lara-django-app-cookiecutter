"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} urls *

:details: {{cookiecutter.project_slug}} urls module.
         - add app specific urls here
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }} <{{ cookiecutter.email }}>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
from django_socio_grpc.settings import grpc_settings
#import lara_django.urls as base_urls

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "{{cookiecutter.project_slug}}"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view


# urlpatterns = [
#     path('', views.EntitySingleTableView.as_view(), name='entity-list-root'),
#     path('entities/list/', views.EntitySingleTableView.as_view(), name='entity-list'),
#     path('addresses/list/', views.AddressesListView.as_view(), name='address-list'),
#     path('addresses/create/', views.AddressCreateView.as_view(),
#          name='address-create'),
#     path('addresses/update/<uuid:pk>', views.AddressUpdateView.as_view(),
#          name='address-update'),
#     path('addresses/delete/<uuid:pk>', views.AddressDeleteView.as_view(),
#          name='address-delete'),
# ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# register handlers in settings
#grpc_settings.user_settings["GRPC_HANDLERS"] += [
#    "{{cookiecutter.project_slug}}.grpc.handlers.grpc_handlers"]
