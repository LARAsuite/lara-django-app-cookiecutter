"""_____________________________________________________________________

:PROJECT: LARAsuite

*{{cookiecutter.project_slug}} views *

:details: {{cookiecutter.project_slug}} views module.
         - add app specific urls here
         - 
:authors: {{ cookiecutter.full_name.replace('\"', '\\\"') }} <{{ cookiecutter.email }}>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

# from .forms import
# from .tables import

# Create your  {{cookiecutter.project_slug}} views here.


@dataclass
class ItemMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [
        {'name': 'item1',
         'path': '{{ cookiecutter.app_name_short}}:view1-name'},
        {'name': 'item2',
         'path': '{{ cookiecutter.app_name_short}}:view2-name'},
    ])
